# Navi Assistant Extension

Navi Assistant Extension is an innovative tool designed to enhance the application development experience using natural language processing. This extension seamlessly integrates with MIT App Inventor, providing advanced assistance to developers.

## Table of Contents

- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [Features](#features)
- [Contributing](#contributing)
- [License](#license)
- [Contact](#contact)

## Description

Navi Assistant Extension facilitates app creation by assisting users through natural language interactions. It offers app evaluations, troubleshooting during development, and step-by-step guides to learn the platform. It supports multiple languages, including English and Spanish.

## Installation

Follow these steps to install and set up the extension in your browser:

1. Clone this repository:
    ```bash
    git clone https://gitlab.com/navi-assistant/Extension.git
    ```
2. Navigate to the project directory:
    ```bash
    cd navi_assistant_extension
    ```
3. Open your browser and go to `chrome://extensions/`.
4. Enable "Developer mode" in the top right corner.
5. Click on "Load unpacked" and select the project folder.

## Usage

Once installed, the extension is ready to use. Available functionalities include:

- **App Evaluation**: Get a comprehensive evaluation of your app and suggestions for improvement.
- **Troubleshooting**: Receive real-time help to resolve development issues.
- **Step-by-Step Guides**: Develop apps from scratch with detailed guides that accompany you through each step.

## Features

- **Automatic Integration with MIT App Inventor**: Navigate and develop apps directly from your browser.
- **Multilingual Support**: Communicate with Navi in English and Spanish.
- **Improve Specific Areas**: Request recommendations to enhance specific aspects of your app.
- **Natural Language Interaction**: Converse with Navi as if you were talking to a real person to get help and suggestions.

## Contributing

Contributions are welcome! To contribute:

1. Fork the project.
2. Create a new branch (`git checkout -b feature-new-feature`).
3. Make your changes.
4. Commit your changes (`git commit -m 'Add new feature'`).
5. Push to the branch (`git push origin feature-new-feature`).
6. Open a Pull Request.

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

For any questions or inquiries, you can contact us through:

- **Email**: ruben.baena@uca.es
- **Website**: [Navi Assistant](https://naviassistant.com/)
